package triplet.petros.com.triplet.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Random;

import triplet.petros.com.triplet.Data.Variables;
import triplet.petros.com.triplet.MainActivity;
import triplet.petros.com.triplet.R;
import triplet.petros.com.triplet.Utilities.utils;

public class GameActivity extends Fragment implements View.OnClickListener  {
    private View myView;
    private MainActivity main;
    ImageView icon1, icon2, icon3, icon4, icon5, icon6, icon7, icon8, icon9;
    TextView timerText;
    TextView playText,backText;
    TextView player1Text,player2Text;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.game_fragment, container, false);

        timerText = (TextView)myView.findViewById(R.id.playeTurnText);
        playText  = (TextView)myView.findViewById(R.id.newGameBut);
        backText  = (TextView)myView.findViewById(R.id.backBut);

        player1Text = (TextView)myView.findViewById(R.id.player1Text);
        player2Text = (TextView)myView.findViewById(R.id.player2Text);

        player1Text.setText(Variables.player1Name);
        player2Text.setText(Variables.player2Name);

        Variables.setPlayerPawns(1);
        Variables.setEnemyPawns(2);


        inisializeView();
        Variables.setPlayer1Turn(new Random().nextBoolean());
        if(!Variables.isPlayer1Turn()){
            Variables.setPlayer2Turn(true);
        }else{
            Variables.setPlayer2Turn(false);
        }
        return myView;
    }


    public void inisializeView() {
        Variables.resetGame();

        icon1 = (ImageView) myView.findViewById(R.id.icon1);
        icon2 = (ImageView) myView.findViewById(R.id.icon2);
        icon3 = (ImageView) myView.findViewById(R.id.icon3);
        icon4 = (ImageView) myView.findViewById(R.id.icon4);
        icon5 = (ImageView) myView.findViewById(R.id.icon5);
        icon6 = (ImageView) myView.findViewById(R.id.icon6);
        icon7 = (ImageView) myView.findViewById(R.id.icon7);
        icon8 = (ImageView) myView.findViewById(R.id.icon8);
        icon9 = (ImageView) myView.findViewById(R.id.icon9);

        ImageView[] views = {icon1, icon2, icon3, icon4, icon5, icon6, icon7, icon8, icon9};
        Variables.setViews(views);

        setListeners(backText);
        setListeners(playText);

        for (int i = 0; i < views.length; i++) {
            setListeners(Variables.getViews()[i]);
        }

    }
    public void setListeners(TextView views) {
        views.setOnClickListener(this);

    }
    public void setListeners(ImageView views) {
        views.setOnClickListener(this);

    }

    public void checkStatus(ImageView icon, int iconStatus) {
        switch (iconStatus) {
            case 0:
                break;
            case 1:
                icon.setBackgroundResource(R.drawable.player2icon);
                break;
            case 2:
                icon.setBackgroundResource(R.drawable.player1icon);
                break;
            default:
                break;
        }
    }

    ///Beating the Game Screen
    public void beatGame() {
        Variables.setIsGameOver(true);
        Variables.setWinner("Player1");
        utils.setFragment(main,2);
    }
    //Game Over Screen
    public void gameOver() {
        Variables.setIsGameOver(true);
        Variables.setWinner("Player2");
        utils.setFragment(main, 3);
    }

    //check winning and loosing conditions
    public void checkWin() {
        Log.i("GameActivity",""+Variables.printStatus());
        //xxx
        //***
        //***
        if (Variables.getStatus()[0] == Variables.getPlayerPawns() && Variables.getStatus()[1] == Variables.getPlayerPawns() && Variables.getStatus()[2] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //x**
        //x**
        //x**
        if (Variables.getStatus()[0] == Variables.getPlayerPawns() && Variables.getStatus()[3] == Variables.getPlayerPawns() && Variables.getStatus()[6] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //x**
        //*x*
        //**x
        if (Variables.getStatus()[0] == Variables.getPlayerPawns() && Variables.getStatus()[5] == Variables.getPlayerPawns() && Variables.getStatus()[8] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //***
        //***
        //xxx
        if (Variables.getStatus()[6] == Variables.getPlayerPawns() && Variables.getStatus()[7] == Variables.getPlayerPawns() && Variables.getStatus()[8] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //**x
        //**x
        //**x
        if (Variables.getStatus()[2] == Variables.getPlayerPawns() && Variables.getStatus()[5] == Variables.getPlayerPawns() && Variables.getStatus()[8] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //*x*
        //*x*
        //*x*
        if (Variables.getStatus()[1] == Variables.getPlayerPawns() && Variables.getStatus()[4] == Variables.getPlayerPawns() && Variables.getStatus()[7] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //***
        //xxx
        //***
        if (Variables.getStatus()[3] == Variables.getPlayerPawns() && Variables.getStatus()[4]== Variables.getPlayerPawns() && Variables.getStatus()[5] == Variables.getPlayerPawns()) {
            beatGame();
        }
        //**x
        //*x*
        //x**
        if (Variables.getStatus()[6] == Variables.getPlayerPawns() && Variables.getStatus()[4]== Variables.getPlayerPawns() && Variables.getStatus()[2] == Variables.getPlayerPawns()) {
            beatGame();
        }

        //xxx
        //***
        //***
        if (Variables.getStatus()[0] == Variables.getEnemyPawns() && Variables.getStatus()[1] == Variables.getEnemyPawns() && Variables.getStatus()[2] == Variables.getEnemyPawns()) {

            gameOver();
        }
        //x**
        //x**
        //x**
        if (Variables.getStatus()[0] == Variables.getEnemyPawns() && Variables.getStatus()[3] == Variables.getEnemyPawns() && Variables.getStatus()[6] == Variables.getEnemyPawns()) {
            gameOver();
        }
        //x**
        //*x*
        //**x
        if (Variables.getStatus()[0] == Variables.getEnemyPawns() && Variables.getStatus()[4] == Variables.getEnemyPawns() && Variables.getStatus()[8] == Variables.getEnemyPawns()) {
            gameOver();
        }
        //***
        //***
        //xxx
        if (Variables.getStatus()[6] == Variables.getEnemyPawns() && Variables.getStatus()[7] == Variables.getEnemyPawns() && Variables.getStatus()[8] == Variables.getEnemyPawns()) {
            gameOver();
        }
        //**x
        //**x
        //**x
        if (Variables.getStatus()[2] == Variables.getEnemyPawns() && Variables.getStatus()[5] == Variables.getEnemyPawns() && Variables.getStatus()[8] == Variables.getEnemyPawns()) {
            gameOver();
        }

        //*x*
        //*x*
        //*x*
        if (Variables.getStatus()[1] == Variables.getEnemyPawns() && Variables.getStatus()[4] == Variables.getEnemyPawns() && Variables.getStatus()[7] == Variables.getEnemyPawns()) {
            gameOver();
        }
        //***
        //xxx
        //***
        if (Variables.getStatus()[3] == Variables.getEnemyPawns() && Variables.getStatus()[4]== Variables.getEnemyPawns() && Variables.getStatus()[5] == Variables.getEnemyPawns()) {
            gameOver();
        }
        //**x
        //*x*
        //x**
        if (Variables.getStatus()[6] == Variables.getEnemyPawns() && Variables.getStatus()[4]== Variables.getEnemyPawns() && Variables.getStatus()[2] == Variables.getEnemyPawns()) {
            gameOver();
        }

//        for(int i = 0; i < Variables.status.length;i++){
//            if(Variables.statusFilled()){
//                Variables.resetGame();
////                gameOver();
//            }
//        }

    }

    //OnClickListeners Method

    public void onClick(View v) {
        Variables.lowerSpaces();
        switch (v.getId()) {
            //In case the first space is CLicked
            case R.id.icon1:
                //if its empty prossed
                if(!Variables.checkStatuses(0)) {
                    //check which player turn
                    if (Variables.isPlayer1Turn()) {
                        setIcon(0, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[0], Variables.getPlayerPawns());
                        Log.i("GameActivity", "" + Variables.getViews()[0]);
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(0, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[0], Variables.getEnemyPawns());
                        Log.i("GameActivity", "" + Variables.getViews()[0]);
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }

                break;
            case R.id.icon2:
                if(!Variables.checkStatuses(1)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(1, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[1], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(1, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[1], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon3:

                if(!Variables.checkStatuses(2)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(2, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[2], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(2, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[2], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon4:
                if(!Variables.checkStatuses(3)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(3, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[3], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(3, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[3], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon5:
                if(!Variables.checkStatuses(4)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(4, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[4], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(4, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[4], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon6:
                if(!Variables.checkStatuses(5)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(5, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[5], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(5, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[5], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon7:
                if(!Variables.checkStatuses(6)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(6, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[6], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(6, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[6], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon8:
                if(!Variables.checkStatuses(7)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(7, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[7], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(7, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[7], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.icon9:
                if(!Variables.checkStatuses(8)) {
                    if (Variables.isPlayer1Turn()) {
                        setIcon(8, Variables.getPlayerPawns());
                        checkStatus(Variables.getViews()[8], Variables.getPlayerPawns());
                        timerText.setText(Variables.changePlayer1turn());
                    } else {
                        setIcon(8, Variables.getEnemyPawns());
                        checkStatus(Variables.getViews()[8], Variables.getEnemyPawns());
                        timerText.setText(Variables.changePlayer2turn());
                    }
                }
                break;
            case R.id.backBut:
                Variables.resetGame();
                utils.setFragment(main,0);
                break;
            case R.id.newGameBut:
                Variables.resetGame();
                utils.setFragment(main, 4);
                break;

        }
        new Thread(new Runnable() {
            public void run() {
                checkWin();
            }
        }).start();

    }

    public void setIcon(int index, int pawn) {

        switch (index) {
            case 0:
                Variables.getStatus()[index] = pawn;
                break;
            case 1:
                Variables.getStatus()[index] = pawn;
                break;
            case 2:
                Variables.getStatus()[index] = pawn;
                break;
            case 3:
                Variables.getStatus()[index] = pawn;
                break;
            case 4:
                Variables.getStatus()[index] = pawn;
                break;
            case 5:
                Variables.getStatus()[index] = pawn;
                break;
            case 6:
                Variables.getStatus()[index] = pawn;
                break;
            case 7:
                Variables.getStatus()[index] = pawn;
                break;
            case 8:
                Variables.getStatus()[index] = pawn;
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            main = (MainActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ExampleFragmentCallbackInterface ");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        main = null;
    }
}

package triplet.petros.com.triplet.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import triplet.petros.com.triplet.Data.Variables;
import triplet.petros.com.triplet.MainActivity;
import triplet.petros.com.triplet.R;
import triplet.petros.com.triplet.Utilities.utils;


public class game_menu extends Fragment implements View.OnClickListener {

    private static final String TAG = "Game_Menu_Fragment";


    private MainActivity main;
    private View myFragmetnView;

    private EditText player1Name,player2Name;
    private Button startGameButton,backButton;



    public game_menu() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static game_menu newInstance(String param1, String param2) {
        game_menu fragment = new game_menu();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmetnView =  inflater.inflate(R.layout.fragment_game_menu, container, false);

        initialiseViews();


        return myFragmetnView;
    }

    public void initialiseViews(){
        player1Name = (EditText)myFragmetnView.findViewById(R.id.player1Name);
        player2Name= (EditText)myFragmetnView.findViewById(R.id.player2Name);
        startGameButton  = (Button)myFragmetnView.findViewById(R.id.startButton);
        backButton = (Button)myFragmetnView.findViewById(R.id.backButton);
        player1Name.setHint("" + Variables.getPlayer1Name());
        player2Name.setHint("" + Variables.getPlayer2Name());

        utils.setListeners(this,player1Name);
        utils.setListeners(this, player2Name);
        utils.setListeners(this,startGameButton);
        utils.setListeners(this, backButton);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startButton:
                Variables.setPlayer1Name("" + player1Name.getText());
                Variables.setPlayer2Name("" + player2Name.getText());
                utils.setFragment(main, 1);
                Variables.setIsGameOver(false);
                break;
            case R.id.backButton:
                utils.setFragment(main, 0);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            main = (MainActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ExampleFragmentCallbackInterface ");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        main = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

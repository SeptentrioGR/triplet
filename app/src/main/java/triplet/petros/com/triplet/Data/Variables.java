package triplet.petros.com.triplet.Data;

import android.media.Image;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Peter on 3/3/2016.
 */
public class Variables {

    static int icon1Status;
    static int icon2Status;
    static int icon3Status;
    static int icon4Status;
    static int icon5Status;
    static int icon6Status;
    static int icon7Status;
    static int icon8Status;
    static int icon9Status;

    //Player 1 and Player 2 Variables
    public static int playerPawns;
    public static int enemyPawns;

    public static boolean player1Turn;
    public static boolean player2Turn;

    public static ImageView player1Pawn;

    public static void setPlayer1Pawn(ImageView player1Pawn) {
        Variables.player1Pawn = player1Pawn;
    }

    public static void setPlayer2Pawm(ImageView player2Pawm) {
        Variables.player2Pawm = player2Pawm;
    }

    public static ImageView player2Pawm;

    public static ImageView getPlayer2Pawm(){
        if(getEnemyPawns() == 1 ){
            return player1Pawn;
        }else if(getEnemyPawns()==2){
            return player2Pawm;
        }
        return player1Pawn;
    }
    public static ImageView getPlayer1Pawn(){
        if(getPlayerChoosePawns() == 1 ){
            return player1Pawn;
        }else if(getPlayerChoosePawns()==2){
            return player2Pawm;
        }
        return player1Pawn;
    }

    public static String getPlayer1Name() {
        return player1Name;
    }

    public static void setPlayer1Name(String player1Name) {
        Variables.player1Name = player1Name;
    }

    public static String getPlayer2Name() {
        return player2Name;
    }

    public static void setPlayer2Name(String player2Name) {
        Variables.player2Name = player2Name;
    }

    public static String player1Name;
    public static String player2Name;


    public static float timer;




    public static float getTimer() {
        return timer;
    }

    public static void setTimer(float timer) {
        Variables.timer = timer;
    }

    public static boolean isGameOver() {
        return isGameOver;
    }

    public static void setIsGameOver(boolean newisGameOver) {
        isGameOver = newisGameOver;
    }

    public static int getSpaces() {
        return spaces;
    }

    public static void setSpaces(int spaces) {
        Variables.spaces = spaces;
    }

    static boolean  isGameOver = false;

    public static String getWinner() {
        return winner;
    }

    public static void setWinner(String winner) {
        Variables.winner = winner;
    }

    public static String winner;

    public static int spaces = 8;

    public static int[] status={icon1Status,icon2Status,icon3Status,icon4Status,icon5Status,icon6Status,icon7Status,icon8Status,icon9Status};
    public static ImageView[] views;


    public static void lowerSpaces(){
        spaces--;
    }

    public static boolean checkStatuses(int index){
            int num = status[index];
        return num == 1 || num == 2;

    }
    public static boolean statusFilled(){
        return spaces < 0;
    }
    public static String changePlayer2turn(){
        Variables.setPlayer1Turn(true);
        Variables.setPlayer2Turn(false);
        return player1Name + " turn ";
    }
    public static String changePlayer1turn(){
        Variables.setPlayer1Turn(false);
        Variables.setPlayer2Turn(true);
        return player2Name + " turn ";
    }
    public static void resetGame(){

        for(int i = 0 ; i < status.length;i++){
            status[i] = 0;
        }

    }
    public static int printStatus(){
        for (int statu : status) {
            return statu;
        }
        return 0;
    }
    public static int getPlayerPawns() {
        return playerPawns;
    }

    public static void setPlayerPawns(int playerPawns) {
        Variables.playerPawns = playerPawns;
    }

    public static int getEnemyPawns() {
        return enemyPawns;
    }

    public static void setEnemyPawns(int enemyPawns) {
        Variables.enemyPawns = enemyPawns;
    }


    public static int getIcon1Status() {
        return icon1Status;
    }

    public static void setIcon1Status(int icon1Status) {
        Variables.icon1Status = icon1Status;
    }

    public static int getIcon2Status() {
        return icon2Status;
    }

    public static void setIcon2Status(int icon2Status) {
        Variables.icon2Status = icon2Status;
    }

    public static int getIcon3Status() {
        return icon3Status;
    }

    public static void setIcon3Status(int icon3Status) {
        Variables.icon3Status = icon3Status;
    }

    public static int getIcon4Status() {
        return icon4Status;
    }

    public static void setIcon4Status(int icon4Status) {
        Variables.icon4Status = icon4Status;
    }

    public static int getIcon5Status() {
        return icon5Status;
    }

    public static void setIcon5Status(int icon5Status) {
        Variables.icon5Status = icon5Status;
    }

    public static int getIcon6Status() {
        return icon6Status;
    }

    public static void setIcon6Status(int icon6Status) {
        Variables.icon6Status = icon6Status;
    }

    public static int getIcon7Status() {
        return icon7Status;
    }

    public static void setIcon7Status(int icon7Status) {
        Variables.icon7Status = icon7Status;
    }

    public static int getIcon8Status() {
        return icon8Status;
    }

    public static void setIcon8Status(int icon8Status) {
        Variables.icon8Status = icon8Status;
    }

    public static int getIcon9Status() {
        return icon9Status;
    }

    public static void setIcon9Status(int icon9Status) {
        Variables.icon9Status = icon9Status;
    }

    public static int[] getStatus() {
        return status;
    }

    public static void setStatus(int[] status) {
        Variables.status = status;
    }

    public static ImageView[] getViews() {
        return views;
    }

    public static void setViews(ImageView[] views) {
        Variables.views = views;
    }

    public static int getPlayerChoosePawns() {
        return playerPawns;
    }

    public static void setPlayerChoosePawns(int playerChoosePawns) {
       playerPawns = playerChoosePawns;
    }

    public static boolean isPlayer1Turn() {
        return player1Turn;
    }

    public static void setPlayer1Turn(boolean playerTurn) {
        Variables.player1Turn = playerTurn;
    }
    public static boolean isPlayer2Turn() {
        return player2Turn;
    }

    public static void setPlayer2Turn(boolean player2Turn) {
        Variables.player2Turn = player2Turn;
    }
}

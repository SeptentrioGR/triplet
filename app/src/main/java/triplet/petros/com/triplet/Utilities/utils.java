package triplet.petros.com.triplet.Utilities;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import triplet.petros.com.triplet.R;
import triplet.petros.com.triplet.fragments.BlankFragment;
import triplet.petros.com.triplet.fragments.GameActivity;
import triplet.petros.com.triplet.fragments.LoseActivity;
import triplet.petros.com.triplet.fragments.game_menu;
import triplet.petros.com.triplet.fragments.winActivity;

public class utils {

    public static Fragment menuFragment;
    public static Fragment gameActivity;
    public static Fragment loseActivity;
    public static Fragment winAct;
    public static Fragment gameMenuAct;
    //Change Fragment Method

    public static void initFragments(){
        menuFragment = new BlankFragment();
        gameActivity = new GameActivity();
        winAct = new winActivity();
        loseActivity = new LoseActivity();
        gameMenuAct = new game_menu();
    }
    public static void setListeners(View.OnClickListener act,TextView views) {
        views.setOnClickListener(act);

    }
    public static void setListeners(View.OnClickListener act,ImageView views) {
        views.setOnClickListener(act);

    }
    public static void changeFragment(FragmentActivity mainActivity,Fragment newActivity,String Text){
        //Starts the transition to the new Fragment

        FragmentTransaction trans = mainActivity.getSupportFragmentManager().beginTransaction();
        //Initialise the new fragment we want to change
        trans.replace(R.id.fragment_container, newActivity);
        //add the the Back Stack so we can return to this if we want to
        trans.addToBackStack("Text");
        //Commit to the replace of the fragment
        trans.commit();
    }

    public static void setFragment(FragmentActivity mainActivity,int index){
        switch (index){
            case 0:
                changeFragment(mainActivity, menuFragment,"Menu");
                break;
            case 1:
                changeFragment(mainActivity,gameActivity,"Game");
                break;
            case 2:
                changeFragment(mainActivity,winAct,"Win");
                break;
            case 3:
                changeFragment(mainActivity,loseActivity,"Lose");
                break;
            case 4:
                changeFragment(mainActivity,gameMenuAct,"Setup");
                break;
        }

    }


}

package triplet.petros.com.triplet;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;


import triplet.petros.com.triplet.Utilities.utils;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    int mNavItemid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        utils.initFragments();
        utils.setFragment(this, 0);




        drawerLayout = (DrawerLayout)findViewById(R.id.navLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toobar);
        setSupportActionBar(toolbar);


        navigationView = (NavigationView)findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);


        if( getIntent().getBooleanExtra("Exit me", false)){
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.drawer_item_1:
                // User chose the "Settings" item, show the app settings UI...
                Toast.makeText(this, "Action_Bar_Item_1", Toast.LENGTH_SHORT);
                drawerLayout.closeDrawer(GravityCompat.START);
                utils.setFragment(this,0);
                return true;

            case R.id.drawer_item_2:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                Toast.makeText(this, "Action_Bar_Item_2", Toast.LENGTH_SHORT);
                utils.setFragment(this, 4);
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}

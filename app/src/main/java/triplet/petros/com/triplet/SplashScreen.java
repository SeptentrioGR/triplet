package triplet.petros.com.triplet;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Space;

import triplet.petros.com.triplet.Data.Variables;
import triplet.petros.com.triplet.Utilities.utils;

public class SplashScreen extends AppCompatActivity {
    int splashInterval = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Check if root exist

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                new Thread(new Runnable() {
                    public void run() {
                        Variables.setPlayer1Name("Choose Player 1 Name");
                        Variables.setPlayer2Name("Choose Player 2 Name");


                        Intent mainActivity = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(mainActivity);
                        finish();
                    }
                }).start();
            }

        }, splashInterval);

    }



}

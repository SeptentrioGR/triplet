package triplet.petros.com.triplet.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import triplet.petros.com.triplet.Data.Variables;
import triplet.petros.com.triplet.MainActivity;
import triplet.petros.com.triplet.R;
import triplet.petros.com.triplet.Utilities.utils;

/**
 * Created by Peter on 3/2/2016.
 */
public class winActivity extends Fragment implements View.OnClickListener {
    private TextView titleText;

    private MainActivity main;
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.won_fragment, container, false);
        Button winButton = (Button) myView.findViewById(R.id.myButton);
        Button exitButton = (Button) myView.findViewById(R.id.ExitButWin);
        winButton.setOnClickListener(this);
        exitButton.setOnClickListener(this);
        TextView winingText = (TextView) myView.findViewById(R.id.textView);
        winingText.setText("Congratulation!" + "\n" + "Winner: " + Variables.player1Name);

        return myView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myButton:
                Toast.makeText(main,"Replay",Toast.LENGTH_SHORT).show();
                utils.setFragment(main,1);
                Variables.resetGame();
                break;
            case R.id.ExitButWin:
                Toast.makeText(main,"Exit",Toast.LENGTH_SHORT).show();
                utils.setFragment(main, 0);
                Variables.resetGame();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            main = (MainActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ExampleFragmentCallbackInterface ");
        }
    }
}
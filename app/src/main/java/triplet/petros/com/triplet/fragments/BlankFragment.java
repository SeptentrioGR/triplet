package triplet.petros.com.triplet.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import triplet.petros.com.triplet.Data.Variables;
import triplet.petros.com.triplet.MainActivity;
import triplet.petros.com.triplet.R;
import triplet.petros.com.triplet.Utilities.utils;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment implements View.OnClickListener{

    private MainActivity main;

    private View myView;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            main = (MainActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ExampleFragmentCallbackInterface ");
        }
    }

    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_blank, container, false);
        Variables.setIsGameOver(true);
        initButtons();
        return myView;

    }


    public void initButtons(){
        Button playButton = (Button) myView.findViewById(R.id.PlayBut);
        Button exitButton = (Button) myView.findViewById(R.id.ExitBut);
        playButton.setOnClickListener(this);
        exitButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.PlayBut:
                Toast.makeText(main, "Play", Toast.LENGTH_SHORT).show();
                utils.setFragment(main, 4);
                Variables.setIsGameOver(false);
                break;
            case R.id.ExitBut:
                Toast.makeText(main, "Exit", Toast.LENGTH_SHORT).show();
                main.finish();
                System.exit(0);
                break;

        }
    }
}
